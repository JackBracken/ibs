package main

import (
	"errors"
	"fmt"

	"github.com/fanatic/go-infoblox"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	host string
	password string
	username string
)

type Conf struct {
	Host string
	Username string
	Password string
}

func main() {
	cmdListNetworks := &cobra.Command{
		Use:   "list-networks",
		Aliases: []string{"networks"},
		Short: "List all networks",
		Long:  "List all of the networks in Infoblox along with their WAPI references.",
		Args:  cobra.ExactArgs(0),

		Run: func(cmd *cobra.Command, args []string) {
			listNetworks()
		},
	}

	cmdGetNetwork := &cobra.Command{
		Use:   "get-network network",
		Aliases: []string{"network"},
		Short: "Print out a description of the given network",
		Long:  `Print out a description of the given network. Network can be a network in
CIDR notation or a WAPI reference.`,
		Args:  cobra.ExactArgs(1),

		Run: func(cmd *cobra.Command, args []string) {
			getNetwork(args[0])
		},
	}

	cmdLookup:= &cobra.Command{
		Use:   "lookup object...",
		Aliases: []string{"info"},
		Short: "Look up objects in the Infoblox API",
		Long:  `Look up objects in the Infoblox API and print out a description of each
of them. An object can be an IP address, a hostname or an Infoblox WAPI
reference.`,
		Args:  cobra.MinimumNArgs(1),

		Run: func(cmd *cobra.Command, args []string) {
			lookupObjects(args)
		},
	}

	// cmdGetGrid := &cobra.Command{
	// 	Use:   "",
	// 	Aliases: []string{""},
	// 	Short: "",
	// 	Long:  "",
	// 	Args:  cobra.ExactArgs(0),
	//
	// 	Run: func(cmd *cobra.Command, args []string) {
	// 		fmt.Println("looking up IP address!")
	// 	},
	// }

	cmdNextIP:= &cobra.Command{
		Use:   "next-ip network",
		Short: "Print the next available IP address(es) in the given network",
		Long:  `Print the next available IP address(es) in the given network. A network
can be a network in CIDR notation or a WAPI reference.`,
		Args:  cobra.ExactArgs(1),

		Run: func(cmd *cobra.Command, args []string) {
			var count = 1
			if cmd.Flags().Lookup("count") != nil {
				count, _ = cmd.Flags().GetInt("count")
			}
			nextAvailableIP(args[0], count)
		},
	}
	cmdNextIP.Flags().Int("count", 1, "the number of IP addresses to return")

	rootCmd := &cobra.Command{Use: "ibs"}

	// ibs will prefer configuration in the following order
	// 1. command-line flags
	// 2. environment variables
	// 3. a config file passed in by command line
	// 4. local config file
	// 5. global config file
	viper.SetConfigName("ibs")
	viper.SetConfigType("yaml")

	viper.AddConfigPath(".")
	home, err := homedir.Dir()
	if err == nil {
		viper.AddConfigPath(home)
	}

	err = viper.ReadInConfig()
	if err != nil {
		// TODO: add verbose flag and print warning here
		// fmt.Println(err)
	}

	viper.SetEnvPrefix("infoblox")
	viper.AutomaticEnv()

	rootCmd.PersistentFlags().StringP("host", "s","", "the Infoblox server to connect to")
	rootCmd.PersistentFlags().StringP("password", "p", "", "the password for the Infoblox user")
	rootCmd.PersistentFlags().StringP("username", "u","", "the user used to connect to Infoblox")

	viper.BindPFlag("host", rootCmd.PersistentFlags().Lookup("host"))
	viper.BindPFlag("password", rootCmd.PersistentFlags().Lookup("password"))
	viper.BindPFlag("username", rootCmd.PersistentFlags().Lookup("username"))

	// We read from viper.settings, not the conf. D'oh.
	var c Conf
	viper.Unmarshal(&c)

	rootCmd.AddCommand(cmdListNetworks, cmdGetNetwork, cmdLookup, cmdNextIP)
	if rootCmd.Execute() != nil {
		fmt.Println(err)
	}
}

func buildClient() (*infoblox.Client, error) {
	host = viper.GetString("host")
	username = viper.GetString("username")
	password = viper.GetString("password")
	if host == "" || username == "" || password == "" {
		return nil, errors.New("ibs: incomplete Infoblox configuration")
	}

	// fmt.Println(host, username, password)

	// TODO: add flags for sslVerify and useCookies
	client := infoblox.NewClient(host, username, password, false, false)

	return client, nil
}