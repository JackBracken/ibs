package main

import (
	"fmt"
	"os"

	"github.com/fanatic/go-infoblox"
)

// TODO: everything :sob:
func listNetworks() {
	ib, err := buildClient()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	networks, err := ib.Network().All(nil)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for _, v := range networks {
		// TODO: Print something more useful
		fmt.Println(v["network"])
	}
}

func getNetwork(network string) {
	ib, err := buildClient()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	obj := networkObject(ib, network)
	opts := infoblox.Options{
		ReturnFields: []string{""},
		ReturnBasicFields: true,
	}

	// TODO: consolidate fields
	// https://ipam.illinois.edu/wapidoc/objects/network.html
	fmt.Println(obj.Get(&opts))
}

func networkObject(ib *infoblox.Client, network string) *infoblox.NetworkObject {
	term := "network"
	query := []infoblox.Condition{{
		Field: &term,
		Value: network,
	},
	}

	out, err := ib.Network().Find(query, nil)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if len(out) != 1 {
		fmt.Println("network not found")
		os.Exit(1)
	}

	return ib.NetworkObject(out[0]["_ref"].(string))
}

func lookupObjects(args []string) {
	for _, v := range args {
		fmt.Println("lookup object:", v)
	}
}

func nextAvailableIP(network string, count int) {
}